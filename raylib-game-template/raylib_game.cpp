#include "raylib.h"

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 800;
    const int screenHeight = 450;

    InitWindow(screenWidth, screenHeight, "raylib [core] example - keyboard input");

    Vector2 p_pos = { 400, 225 };
    Vector2 p_size = {32, 32};
    Texture2D p_tex = LoadTexture("player/1.png");

    float speed = 5.0;
    Camera2D camera = { 0 };
    //camera.target = (Vector2){ p_pos.x + 20.0f, p_pos.y + 20.0f };
    camera.offset = (Vector2){ screenWidth/2.0f, screenHeight/2.0f };
    camera.rotation = 0.0f;
    camera.zoom = 1.0f;

    SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        camera.target = (Vector2){ p_pos.x + 20, p_pos.y + 20 };
        //----------------------------------------------------------------------------------
        if (IsKeyDown(KEY_RIGHT)) p_pos.x += speed;
        if (IsKeyDown(KEY_LEFT)) p_pos.x -= speed;
        if (IsKeyDown(KEY_UP)) p_pos.y -= speed;
        if (IsKeyDown(KEY_DOWN)) p_pos.y += speed;
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(RAYWHITE);

            DrawText("move the ball with arrow keys", 10, 10, 20, DARKGRAY);


            BeginMode2D(camera);

                DrawRectangle(-6000, 320, 13000, 8000, DARKGRAY);

                DrawTextureV(p_tex, p_pos, WHITE);


            EndMode2D();

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}